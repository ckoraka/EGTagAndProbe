 #include <TH2.h>
#include <stdio.h>
#include <TGraph2D.h>
#include <TLatex.h>
#include <TPaveText.h>
#include <TPad.h>
#include <TVirtualPad.h>
#include <TLine.h>
#include "TH1D.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TF1.h"
#include "TH2F.h"
#include "TTree.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TLorentzVector.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TStyle.h"
#include "TChain.h"
#include <iostream>
#include <fstream>
#include "TSystem.h"
#include "TROOT.h"
#include "PlottingTemplate/setTDRStyle_teliko.C"



void plot_resolution_data_vs_MC()
{

	gROOT->LoadMacro("PlottingTemplate/setTDRStyle_teliko.C");
	setTDRStyle_teliko();

	TPaveText *paveCMS = new TPaveText(0.15,0.96,0.93,0.99,"NDC");
	paveCMS->AddText("#bf{CMS #it{Work In Progress}}           Runs 320058-320065   UL 2018 (13 TeV)");
	paveCMS->SetFillColor(0);
	paveCMS->SetBorderSize(0);
	paveCMS->SetTextSize(0.03);
	paveCMS->SetTextFont(42);

	TFile *res_2018 = new TFile("54iov.root");
	TH1D *res_vs_et_barrel_2018= (TH1D*)res_2018->Get("hEt_barrel");
	TH1D *res_vs_et_endcap_2018= (TH1D*)res_2018->Get("hEt_endcap");

	TFile *res_mc = new TFile("246iov.root");
	TH1D *res_vs_et_barrel_mc= (TH1D*)res_mc->Get("hEt_barrel");
	TH1D *res_vs_et_endcap_mc= (TH1D*)res_mc->Get("hEt_endcap");

	TFile *res_unpacked = new TFile("unpacked_54iov.root");
	TH1D *res_vs_et_barrel_unpacked= (TH1D*)res_unpacked->Get("hEt_barrel");
	TH1D *res_vs_et_endcap_unpacked= (TH1D*)res_unpacked->Get("hEt_endcap");

	//////////////////////////////////      Canvases        ///////////////////////////////////////

	TPaveText *l1egb = new TPaveText(.25,.70,.36,.80,"NDC");
	l1egb->AddText("L1 e/#gamma candidates");
	l1egb->AddText("Barrel");
	l1egb->SetTextSize(0.035);
	l1egb->SetFillColor(0);
	l1egb->SetFillStyle(0);
	l1egb->SetBorderSize(0);
	l1egb->SetTextFont(12);
	l1egb->SetTextColor(kBlack);

	TPaveText *l1eg = new TPaveText(.25,.70,.36,.80,"NDC");
	l1eg->AddText("L1 e/#gamma candidates");
	l1eg->AddText("Endcaps");
	l1eg->SetTextSize(0.035);
	l1eg->SetFillColor(0);
	l1eg->SetFillStyle(0);
	l1eg->SetBorderSize(0);
	l1eg->SetTextFont(12);
	l1eg->SetTextColor(kBlack);

	res_vs_et_barrel_2018->SetMarkerColor(kBlack);
	res_vs_et_endcap_2018->SetMarkerColor(kBlack);
	res_vs_et_barrel_2018->SetLineColor(kBlack);
	res_vs_et_endcap_2018->SetLineColor(kBlack);
	res_vs_et_barrel_2018->SetMarkerSize(0.5);
	res_vs_et_endcap_2018->SetMarkerSize(0.5);
	res_vs_et_barrel_2018->SetMarkerStyle(8);
	res_vs_et_endcap_2018->SetMarkerStyle(8);

	res_vs_et_barrel_mc->SetMarkerStyle(8);
	res_vs_et_endcap_mc->SetMarkerStyle(8);
	res_vs_et_barrel_mc->SetMarkerSize(0.5);
	res_vs_et_endcap_mc->SetMarkerSize(0.5); 
	res_vs_et_barrel_mc->SetMarkerColor(kRed+2);
	res_vs_et_endcap_mc->SetMarkerColor(kRed+2);
	res_vs_et_barrel_mc->SetLineColor(kRed+2);
	res_vs_et_endcap_mc->SetLineColor(kRed+2);

	res_vs_et_barrel_unpacked->SetMarkerColor(kBlue+2);
	res_vs_et_endcap_unpacked->SetMarkerColor(kBlue+2);
	res_vs_et_barrel_unpacked->SetLineColor(kBlue+2);
	res_vs_et_endcap_unpacked->SetLineColor(kBlue+2);
	res_vs_et_barrel_unpacked->SetMarkerSize(0.5);
	res_vs_et_endcap_unpacked->SetMarkerSize(0.5);
	res_vs_et_barrel_unpacked->SetMarkerStyle(8);
	res_vs_et_endcap_unpacked->SetMarkerStyle(8);

	TLegend *ll = new TLegend(.65, .7, .88, .85);
	ll->SetTextSize(0.022);
	ll->AddEntry(res_vs_et_barrel_2018,"Twice per week ","lp");
	ll->AddEntry(res_vs_et_barrel_mc,"Once per fill","lp");
	ll->AddEntry(res_vs_et_barrel_unpacked,"Unpacked","lp");
	ll->SetBorderSize(1);

	TLatex *latex = new TLatex();
	latex->SetNDC();
	latex->SetTextSize(0.025);
	latex->SetTextAlign(12); 
	char text[200];

	TCanvas *c4 = new TCanvas("c4","c4");
	c4->cd();
	c4->SetGrid();
	res_vs_et_barrel_2018->Draw("p");
	res_vs_et_barrel_mc->Draw("p same");
	res_vs_et_barrel_unpacked->Draw("p same");
	paveCMS->Draw("same");
	ll->Draw("same");
	l1egb->Draw("same");
	sprintf(text,"#color[1]{Mean: %3.2f , RMS: %3.2f}",res_vs_et_barrel_2018->GetMean(),res_vs_et_barrel_2018->GetRMS());
	latex->DrawLatex(0.18,0.4,text);
	sprintf(text,"#color[2]{Mean: %3.2f , RMS: %3.2f}",res_vs_et_barrel_mc->GetMean(),res_vs_et_barrel_mc->GetRMS());
	latex->DrawLatex(0.18,0.35,text);
	sprintf(text,"#color[4]{Mean: %3.2f , RMS: %3.2f}",res_vs_et_barrel_unpacked->GetMean(),res_vs_et_barrel_unpacked->GetRMS());
	latex->DrawLatex(0.18,0.3,text);
	c4->SaveAs("resolution_et_barrel.pdf");
	c4->SaveAs("resolution_et_barrel.png");

	TCanvas *c5 = new TCanvas("c5","c5");
	c5->cd();
	c5->SetGrid();
	res_vs_et_endcap_unpacked->Draw("p");
	res_vs_et_endcap_mc->Draw("p same");
	res_vs_et_endcap_2018->Draw("p same");
	paveCMS->Draw("same");
	l1eg->Draw("same");
	ll->Draw("same");
	sprintf(text,"#color[1]{Mean: %3.2f , RMS: %3.2f}",res_vs_et_endcap_2018->GetMean(),res_vs_et_endcap_2018->GetRMS());
	latex->DrawLatex(0.18,0.4,text);
	sprintf(text,"#color[2]{Mean: %3.2f , RMS: %3.2f}",res_vs_et_endcap_mc->GetMean(),res_vs_et_endcap_mc->GetRMS());
	latex->DrawLatex(0.18,0.35,text);
	sprintf(text,"#color[4]{Mean: %3.2f , RMS: %3.2f}",res_vs_et_barrel_unpacked->GetMean(),res_vs_et_barrel_unpacked->GetRMS());
	latex->DrawLatex(0.18,0.3,text);
	c5->SaveAs("resolution_et_endcap.pdf");
	c5->SaveAs("resolution_et_endcap.png");

}


